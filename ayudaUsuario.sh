#!/bin/bash

echo -e "---- MANUAL DE AYUDA AL USUARIO ---- \n" 

# Muestra la ayuda acerca de la gestión de usuarios
echo -e "------ Menú Gestión Usuarios ------\n"
echo "-Altas Usuarios: El usuario deberá brindar un archivo con un listado de los usuarios que quiere añadir al sistema. Uno por línea. "
echo -e "Ejemplo de una linea: nombre:apellido:usuario:contraseña  \n"
echo -e "-Cruce de llaves: Se le pedirá al usuario que ingrese un listado con los nombres los usuarios de los cuales desea intercambiar llaves. Luego se le pedirá un listado con las ip de las maquinas donde se encuentran esos usuarios. \n"
echo -e "-Baja Usuarios: usted deberá brindar un archivo con un listado de los usuarios que quiere eliminar del sistema. Uno por linea. \n"
echo "Usuario1"
echo -e "Usuario2 \n"
echo -e "-Reporte de usuarios de sistema: Muestra todos los usuarios del sistema \n"
echo -e "-Reporte de usuarios activos: Muestra todos los usuarios habilitados para login \n"

# Muestra la ayuda acerca de la gestión de discos
echo -e "------ Menú Gestión Discos ------\n"
echo -e "Reporte de información en disco: Muestra información acerca de los discos. \n"
echo -e "Reporte de rendimiento de los discos locales: Muestra información del rendimiento de los discos locales. \n"
echo -e "Reporte de actividad en I/O: Muestra información de entradas y salidas de los discos. \n"

# Muestra la ayuda acerca de la gestión de procesos
echo -e "------ Menú Gestión Procesos ------\n"
echo -e "Reporte de procesos de mayor consumo de memoria: Muestra los 5 procesos que mas memoria están ocupando en el sistema. \n"
echo -e "Verificar cantidad de procesos: Se le pedirá que ingrese un umbral de procesos. En caso de que los procesos superen el umbral se le mostrará una ADVERTENCIA al usuario. \n"
echo -e "Mostrar proceso PID: se le pedirá que ingrese el PID de un proceso, para monitorearlo. \n"
echo -e "Eliminar un proceso: se le pedirá que ingrese el PID de un proceso para luego proceder a finalizarlo. \n"
echo -e "Eliminar lista de procesos: tendrá que ingresar un listado con los PID de todos los procesos que desea eliminar. \n"

# Muestra la ayuda acerca de la gestión de memoria
echo -e "------ Menú Gestión Memoria ------\n"
echo -e "Reporte del uso de memoria RAM: Muestra información acerca del uso de la memoria RAM. \n"
echo -e "Alertas de uso de memoria RAM: Enviá un mail a su correo local en caso de que se haya superado el 20% de uso de memoria ram. "
echo -e "Comando para visualizar su correo local: mail \n"

# Muestra la ayuda acerca de la gestion de texto
echo -e "------ Menú Gestión Texto ------\n"
echo -e "Verificar resolución de hostname: verifica si el hostname de la maquina local se encuentra correctamente configurado en el archivo /etc/hosts \n"
echo -e "Verificar configuración de servidores DNS: se le pedirá que ingrese un listado con las ip de los servidores DNS para verificar si están configurados en el archivo /etc/resolv.conf \n "

echo "------ FIN DEL MANUAL ------"
