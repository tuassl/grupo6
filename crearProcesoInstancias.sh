#!/bin/bash

# Este script ejecuta 10 veces un script que contienen un proceso


for i in {1..10}; do
	./procesoSleep.sh # Ejecuta el script
	echo "Instancia $i del proceso creada"
done

# Guarda los PID de los procesos creados anteriormente y los guarda en un archivo
ps -ef | grep sleep | head -10 | awk '{print $2}' > archivoPID.txt
