# Grupo 6
---------
### Alumno:
- Claudio Azulay
- Franco Carabelli
- Exequiel Mollo
- Kevin Quintero

#### Branch evaluada: master

#Etapas del proyecto

###Menu: 85%
-----
- Le faltaría mejorar el menú general ya que está todo junto y dificulta la lectura al usuario.
- Faltaría también la ayuda de cada sub menú.
- Tenga en cuenta que en los sub menú en algunos casos pone salir y en otros volver.

###Usuarios: 100%
--------
OK!

###Discos: 100%
-------
OK!

###PROCESOS: 100%
---------
OK!

###MEMORIA: 50%
--------
- No modifico el script para poder cambiar el umbral por el usuario para realizar la verificación.,
- No uso mktemp para almacenar la información en el archivo temporal como se solicitó.

###TEXTO: 100%
------
- Cuando se tiene muchas interfaces de red no funciona. (igualmente no saco puntos por eso) pero no era necesario hacerlo
-OK!

#Devolución general:
-------------------
El proyecto está muy bien!. El código está correcto y satisface el objetivo de este trabajo. Como mejora, siempre pensar en la documentación tanto en la cabezara de los scripts como en brindar más ayuda para el usuario.

Puntuación general del trabajo <b>9 pts.</b> Felicitaciones!


