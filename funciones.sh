#!/bin/bash

function esRoot {
      if [[ `id -u` -eq 0 ]];then
          echo
          echo "Acceso concedido"
          echo
          return 0
      else
          echo
          echo "Necesita ser usuario root o tener permisos root"
          echo
          return 1
fi
}

function esEntero {
      expresion=^-?[0-9]+$
      if [[ $1 =~ $expresion ]];then
          echo
          echo "Perfecto, usted ingresó: $1"
          echo
          return 0
      else
          echo
          echo "El argumento dado es incorrecto, usted ingresó: $1 "
          echo
          return 1
      fi
}

function instalado {

      if dpkg -s $1 &> /dev/null ;then
            echo
            echo "Recurso $1 ya instalado, procedemos a ejecutar: "
            echo
            return 0
      else
            echo 
            echo "El paquete $1 no está instalado"
            echo
            echo -n "¿Desea instalar $1? S/N "
            declare -u opcion
            read opcion
            if [ $opcion == "S" ] ;then
                apt-get update
                apt-get install $1
            else
                echo "El paquete no fue instalado" 
                return 1
            fi
      fi
}


function existeProceso {
      if ps aux | awk '{print $2}' | grep $1 > /dev/null ; then
          echo "El proceso $1 existe"
          return 0
      else
          echo "El proceso no existe"
          return 1
      fi
}

function existeUsuario {
      if [ `cat /etc/passwd | grep -w ^$1` ] ; then
          echo
          echo "El usuario $1 ya existe"
          echo
          return 0
      else
          echo
          echo "El usuario $1 NO existe"
          echo
          return 1
     fi
}


function scriptRemoto {
	scriptFunciones=$(find / -type f -iname funciones.sh 2>/dev/null)
	for maquinas in $(cat $1);do
		echo -e "\nEntrando a la maquina $maquinas.. ingrese la contraseña"
		scp $2 $scriptFunciones $3 $maquinas:/tmp > /dev/null
		echo -e "\nVuelva a ingresar la contraseña para poder ejecutar el script.."
		if [[ -f $3 ]]; then
			echo "El archivo de ingresado anteriormente fue transferido al /tmp de la maquina remota. Ingrese la ruta desde ahi, luego de poner la contraseña y volver a seleccionar la opcion."
		fi
		ssh $maquinas "bash /tmp/$2"
	        echo -e "Salio de la maquina $maquinas \n"	
	done
}
