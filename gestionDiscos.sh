#!/bin/bash

source /tmp/funciones.sh 2> /dev/null

function infoDisk () {
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
	
#Crea un archivo donde guarda la información de las particiones dada por el comando df
        df -h | grep  '^/dev/[^ ]*' > /tmp/listado

#Se ejecutará un ciclo que leerá línea por línea
        cat /tmp/listado | while read LINE;do

#Lee la línea e imprime la columna marcada por el comando awk  
        echo "Información  `echo ${LINE} | awk '{print $1}'`"
        echo "Tamaño Total: `echo ${LINE} | awk '{print$ 2}'`"

#Declaramos la variable porcent que toma el valor númerico de la quinta columna
        porcent=` echo ${LINE} | awk '{print $5}'| tr -d %`
        echo "% de espacio utilizado: $porcent% "

#Al estar declarada la variable como un número podemos operar
        echo "% de espacio disponible: `expr 100 - $porcent`% "
        echo -e "Montado en: `echo ${LINE} | awk '{print $6}'` \n "
done
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionDiscos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"	
fi
}

function rendDisk () {
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
#Revisamos si el paquete sysstat esta instalado
        if   esRoot &&  instalado sysstat  ;then
                iostat -d
        else
                return
        fi
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionDiscos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"	
fi

}

function actDisk(){
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
# Evalua si el usuario esta ejecutando la opcion como root
        if  esRoot ;then 

#Revisamos si el paquete iotop esta instalado
                if   instalado iotop ;then
                        echo
                        iotop -o -b -n 1
                else
                        return
                fi
        fi
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionDiscos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"	
fi
}

while true;do
   echo "-----------Gestión de discos----------------"
   echo
   echo "1)Reporte de información en disco"
   echo "2)Reporte rendimiento de los discos locales"
   echo "3)Reporte de actividad en I/O"
   echo "4)Volver"
   echo
   echo "----------------------------------------------"

   read op

   case $op in
      1) infoDisk;;
      2) rendDisk;;
      3) actDisk;;
      4) break;;
      *) echo "Opción inválida, intente otra vez";;
   esac; done
