#!/bin/bash

source /tmp/funciones.sh 2>/dev/null

function usoMem() {
    
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
    	USADO=`free -h | awk '/Mem/{print $3}'`
        TOTAL=`free -h | awk '/Mem/{print $2}'`
	echo "Actualmente esta utilizando $USADO de $TOTAL en: $(hostname)"
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionMemoria.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
	fi
}

function alertaMem() {

declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
#Revisamos si el paquete mailutils esta instalado
        if  esRoot && instalado mailutils ;then       
                ./memory-alert.sh
        else
                return
        fi
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionMemoria.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}


#Submenú de la seccionmemoria

while true;do
   echo "-----------Gestión dememoria----------------"
   echo
   echo "1)Reporte de uso de memoria RAM"
   echo "2)Alertas de uso de memoria RAM"
   echo "3)Volver"
   echo
   echo "----------------------------------------------"

   read op

   case $op in
      1) usoMem;;
      2) alertaMem;;
      3) break;;
      *) echo "Opción inválida, intente otra vez";;
   esac; done

