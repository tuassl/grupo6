#!/bin/bash

source /tmp/funciones.sh 2>/dev/null

declare op  #Variable declarada para ser usada solo en el menu de este shell

function consumoMemoria {

# Esta funcion muestra 5 procesos que mas RAM estan consumiendo

declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
# Comando que lista y guarda en un archivo temporal los procesos y los separa las columnas de cada linea con ":"
ps aux | awk '{print $1, $2, $4, $11}' | sort -k3nr | head -5 | tr " " : > /tmp/temporal.txt

echo "Los 5 procesos que mas RAM estan consumiendo son: "
echo -e "USER    \t     PID    \t     %MEM    \t     COMMAND"

# Por cada linea del archivo se van extrayendo los diferentes campos para luego imprimirlos ordenadamente
for campos in $(cat /tmp/temporal.txt); do

	usr=$(echo $campos | cut -f1 -d:)
	pid=$(echo $campos | cut -f2 -d:)
	mem=$(echo $campos | cut -f3 -d:)
	comando=$(echo $campos | cut -f4 -d:)

	echo -e "$usr    \t     $pid    \t     $mem    \t     $comando "
done
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionProcesos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}


function umbralProcesos {

declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
# Se le pide al usuario que ingrese la cantidad de procesos limite que quiere en el sistema
echo "Ingrese el umbral de procesos para el sistema: "
read umbral

cantProcesosSistema=$(ps aux --no-headers | wc -l)

# Compara la cantidad de procesos del sistema con el umbral dado por el usuario
if [ $umbral -ge $cantProcesosSistema ]; then
	echo "Su umbral de procesos es: $umbral y la cantidad de procesos del sistema son: $cantProcesosSistema"
else
	echo "CUIDADO. La cantidad de procesos superaron el umbral provisto por usted."
	echo "Su umbral es de: $umbral y la cantidad de procesos del sistema son: $cantProcesosSistema"
fi
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionProcesos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}


function monitoreo {
	
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
	echo -n "Por favor ingrese el PID : "
	read num
	if  esEntero $num  ; then
                if [ $num -gt 0 ] && existeProceso $num > /dev/null ;then
		     top -p $num -n 10 -d 2
                else
                     existeProceso $num
                     echo "El número $num no puede ser usado"
                     echo
                fi
	fi
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionProcesos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}

function eliminarProceso { 
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
   if esRoot ; then

echo  "Ingrese el numero (PID) del proceso que desea eliminar: "
read pid
    if existeProceso $pid > /dev/null ; then
       if kill $pid 2> /dev/null && !(existeProceso $pid) > /dev/null ; then
          echo
          echo "El proceso $pid ha sido eliminado"
          echo
       elif echo "El proceso $pid se niega a ser eliminado, tomando medidas" && kill -9 $pid 2> /dev/null && !(existeProceso $pid) > /dev/null ; then
          echo
          echo "El proceso $pid ha sido eliminado de forma forzada"
          echo
       else
          echo "ADVERTENCIA, EL PROCESO $pid NO PUDO SER ELIMINADO"
       fi
    else
       echo 
       echo "El proceso $pid no existe"
       echo
    fi 

   fi 
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionProcesos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}

function killListaProc {
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then

   if esRoot ; then

           echo
           echo -n "Ingrese el nombre del archivo que contiene los PID: "
           read lista
           if [ -f $lista ]; then     #Comprueba si la variable está vacía 
              for proceso in `cat $lista`; do
                  eliminarProceso $proceso
              done
           else
              echo
              echo "El archivo $lista no existe"
              echo
           fi

   fi
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionProcesos.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}


while true; do
	echo "---------Gestión de procesos----------"
	echo
	echo "1) Reporte procesos de mayor consumo de memoria"
	echo "2) Verificar cantidad de procesos"
	echo "3) Mostrar proceso PID"
	echo "4) Eliminar un proceso"
	echo "5) Eliminar una lista de procesos"
	echo "6) Salir"
	echo
	echo "--------------------------------------"

	read op

	case $op in
		1) consumoMemoria;;
		2) umbralProcesos;;
		3) monitoreo;;
		4) eliminarProceso;;
		5) killListaProc;;
		6) break;;
		*) echo "Opción invalida, ingrese un número de 1 a 6";;
	esac; done
