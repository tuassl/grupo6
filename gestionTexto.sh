#!/bin/bash

function hostnameLocal () {

instalado net-tools || return	
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then

# Obtiene la interfaz de red principal
interfaz=$(ifconfig -s | awk '{print $1}' | head -2 | tail -1)

# Obtiene la direccion ipv4 de esa interfaz
ipInter=$(ifconfig $interfaz | grep inet | head -1 | awk '{print $2}')

# Comprueba si en el archivo /etc/hosts existe alguna linea con la direccion ip
# obtenida anteriormente y con hostname 
if [[ `grep $(echo $ipInter) /etc/hosts | grep $(hostname)` ]]; then
        echo "El hostname local se encuentra bien configurado en el archivo /etc/hosts"
else
        echo "ADVERTENCIA."
        echo "El hostname local de la maquina no se encuentra bien configurado en el archivo /etc/hosts"
fi
return
else
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionTexto.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}

function confServDNS {

declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
esRoot || return
read lr
if [[ "$lr" == "L" ]]; then
echo "Ingrese el archivo con las ip de los servidores para verficar si estan configurados en /etc/resolv.conf"
read archivoDNS

declare -u op

sed -i".copia" '/^#/d' /etc/resolv.conf   #Borro los comentarios de /etc/resolv.conf y se crea una copia con el nombre $2.copia

for i in `cat $archivoDNS`;do     #Iterar sobre la lista de IP's de servidores DNS
  cont=0
   for n in `cat /etc/resolv.conf`;do  #Iterar sobre la lista /etc/resolv.conf
      [ `echo $n | cut -f2` == $i ] && let cont=cont+1   #Si el servidor DNS se encuentra en /etc/resolv.conf aumenta el contador en 1
   done
   [ $cont = 0 ] && echo -n "$i no se encuentra en la configuración actual, ¿desea agregarlo? S/N: " && read op && [ $op = "S" ] && sed -i "1i nameserver $i" /etc/resolv.conf
   #Si cont es 0, entonces el servidor DNS no está en resolv.conf, se pregunta para agregarlo y de ser s la respuesta, se agraga en la primera linea
done
   echo "Los servidores se configuraron correctamente."
   cat /etc/resolv.conf 
else
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionTexto.sh
	scriptRemoto $listadoMaquinas $script
	echo -e "\nVolvio a la maquina local"
fi
}


while true;do
   echo "-----------Gestión de texto----------------"
   echo
   echo "1) Verificar resolucion hostname local"
   echo "2) Verificar configuracion servidores DNS"
   echo "3) Volver"
   echo
   echo "----------------------------------------------"

   read op

   case $op in
      1) hostnameLocal;; 
      2) confServDNS;;
      3) break;;
      *) echo "Opción inválida, intente otra vez";;
   esac; done


