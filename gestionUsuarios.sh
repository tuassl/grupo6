#Submenu de la seccion usuarios

#!/bin/bash

source /tmp/funciones.sh 2>/dev/null

function altaDeUsuarios(){

declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
if [ -f $1 ]; #Comprueba si el archivo dado existe
then
	echo "Archivo CSV valido, leyendo..."
else
	echo "Archivo no encontrado"
        exit
fi

for campos in $(cat $1); do
	if ! existeUsuario `echo $campos | cut -f3 -d:` ; then
	   echo "Creando usuario.."

	   nombre=`echo $campos | cut -f1 -d:`
	   apellido=`echo $campos | cut -f2 -d:`
	   usuario=`echo $campos | cut -f3 -d:`
	   password=`echo $campos | cut -f4 -d:`
	
	   useradd -m $usuario -s /bin/bash -k /etc/skel -c "$nombre-$apellido"
	   echo "$usuario:$password" | chpasswd
	
	   echo "Se ha creado el usuario: $usuario"
       fi
done
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionUsuarios.sh
	scriptRemoto $listadoMaquinas $script $1
	echo -e "\nVolvio a la maquina local"
fi
}

function eliminarUsuarios(){

declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
if [ -f $1 ]; #Comprueba si el archivo dado existe
then
	echo "Archivo CSV valido, leyendo..."
else
	echo "Archivo no encontrado"
        return
fi

#Estructura de control que lee cada linea de texto, que contiene un usuario, y borra esos usuarios del sistema
for usuario in $(cat $1); do 
	if existeUsuario $usuario ;then
	    echo "Borrando usuario.."

	    deluser --remove-home $usuario >/dev/null 	
	    echo "Se ha borrado el usuario: $usuario y su directorio /home"
        fi
done
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionUsuarios.sh
	scriptRemoto $listadoMaquinas $script $1
	echo -e "\nVolvio a la maquina local"
fi
}

function cruceLlaves(){
#Esta funcion sirve para cruzar las llaves de los usuarios a las maquinas dadas en un archivo

listado=`cat $1` # Almacena un listado con los nombres de los usuarios
maquinas=`cat $2` # Almacena un listado con las ip de las maquinas con las que hay que cruzar llaves;

for usuario in $listado; do
	# Entramos a la cuenta de un usuario y se ejecutan estos comandos, que comprueban si el usuario tiene claves creadas
	su $usuario --session-command "echo ------------------------------------------------------------;
				       echo Sesion de $usuario; 
				       echo Cuya id es: ;
				       id -u;
				       echo ------------------------------------------------------------;
				       if [ -d /home/$usuario/.ssh ];
				       then echo ------------------------------------------------------------;
					    echo Ya tiene una clave creada;
					    ls -l /home/$usuario/.ssh/ | grep id_r*;
				       else echo No tiene una clave creada. Creando..;
					    ssh-keygen;
					    echo ------------------------------------------------------------;
					    echo Clave creada con exito;
					    echo ------------------------------------------------------------;
					    echo  ;
					    fi"
					    
	# Entramos a cada usuario y ejecutamos el comando para copiar la clave publica				    
	for ip in $maquinas; do
		echo " "
		echo "------------------------------------------------------------"
		echo "Copiando clave del usuario: $usuario a la maquina: $ip"
		echo "------------------------------------------------------------"
		echo " "
		su $usuario --session-command "ssh-copy-id $usuario@$ip"
	done
done

}

#Genera un listado de usuarios de sistema
#La informacion sobre los ID's de los usuarios fue obtenida de /etc/login.defs
#ID's entre 100 y 999, son usuarios del sistema.
function usuariosSistema () {
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
      echo -e "USUARIOS DE SISTEMA \n"
      awk -F : '$3>=100 && $3<=999 {print $1}' /etc/passwd 
      echo ""
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionUsuarios.sh
	scriptRemoto $listadoMaquinas $script 
	echo -e "\nVolvio a la maquina local"
fi
}

#Genera un listado de usuarios habilitados para login
#La informacion sobre los ID's de los usuarios fue obtenida de /etc/login.defs
#ID's entre 1000 y 60000 son usuarios activos, capaces de loguearse.
function usuariosLogin () {
declare -u lr
echo "Desea ejecutar local o remoto (L/R)?"
read lr
if [[ "$lr" == "L" ]]; then
      echo -e "USUARIOS ACTIVOS \n"
      awk -F : '$3>=1000 && $3<=60000 {print $1}' /etc/passwd
      echo ""
else	
	echo -e "\nIngrese un listado con las maquinas en las que desea ejecutar la opcion"
	echo "Ejemplo: usuario@ip"
	read listadoMaquinas
	script=gestionUsuarios.sh
	scriptRemoto $listadoMaquinas $script 
	echo -e "\nVolvio a la maquina local"
fi
}

while true;do
   echo "-------Gestion de usuarios---------"
   echo
   echo "1) Altas de usuarios"
   echo "2) Cruce de llaves"
   echo "3) Baja de usuarios"
   echo "4) Reporte de usuarios de sistema"
   echo "5) Reporte de usuarios activos"
   echo "6) Volver"
   echo
   echo "-----------------------------------"

   read op

   case $op in
      1) if  esRoot ;then 
         echo "ingrese el nombre de un archivo CSV que este escrito en campos separados por ':', ejemplo: "
         echo "nombre:apellido:usuario:contraseña" #Se da un ejemplo de como tendrian que estar compuestas las lineas del archivo CSV
         read archivo
         altaDeUsuarios $archivo
         fi;;
      2) if  esRoot ;then
	     echo "Ingrese el archivo con el listado de los usuarios: "
	     read archivo
	     echo "Ahora ingrese el listado ip de las maquinas con las que desea cruzar las claves de los usuarios dados en el archivo anterior: "
	     read archivoIP
	     cruceLlaves $archivo $archivoIP
         fi;;
      3) if  esRoot ;then
         echo
         echo "ADVERTENCIA, BORRARA USUARIOS ¿QUIERE CONTINUAR? S/N"
         declare -u respuesta
         read respuesta
           if [ "$respuesta" == "S" ] ;then
               echo "ingrese el nombre de un archivo CSV: "
               read archivo
               eliminarUsuarios $archivo
           else
               return
           fi
         fi;;
      4) usuariosSistema ;;
      5) usuariosLogin ;;
      6) break;;
      *) echo "Opcion invalida, intente otra vez";;
   esac; done 
