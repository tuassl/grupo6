#!/bin/bash
ramusage=$(free | awk '/Mem/{printf $3/$2*100}')

if [[ $ramusage > 20 ]]; then
        SUBJECT="ATENCI�~SN: El uso de memoria es alto en $(hostname) at $(date)"
        MESSAGE="/tmp/Mail.out"
        TO="$USER"

        echo "La cantidad de memoria en uso es: $ramusage%" >> $MESSAGE
        echo "" >> $MESSAGE
        echo "------------------------------------------------------------------" >> $MESSAGE
        echo "Consumo de memoria con información de TOP" >> $MESSAGE
        echo "------------------------------------------------------------------" >> $MESSAGE
        echo "$(top -b -o +%MEM | head -n 20)" >> $MESSAGE
        echo "" >> $MESSAGE
        echo "------------------------------------------------------------------" >> $MESSAGE
        echo "Consumo de memoria con información de PS" >> $MESSAGE
        echo "------------------------------------------------------------------" >> $MESSAGE
        echo "$(ps -eo pid,ppid,%mem,%cpu,cmd --sort=-%mem | head)" >> $MESSAGE
        mail -s "$SUBJECT" "$TO" < $MESSAGE
        rm /tmp/Mail.out
        echo "Se ha enviado el reporte de alerta a su correo"
else
        echo "El uso de memoria RAM esta por debajo del 20%, no es necesario enviar alerta. "
fi

