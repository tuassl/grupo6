#!/bin/bash

#Script de prueba para el menú

#Muestra el menú hasta que el usuario decida salir, eligiendo la opción 6.
#Si elige una opción que no sea 1-6, se le informa que es una opción inválida y se le permite volver a intentarlo.

. funciones.sh

if [[ $1 == "-h" ]]; then
	source ayudaUsuario.sh | more
	exit
fi

while true;do
   echo
   echo "----------------MENÚ----------------"
   echo
   echo "1) Gestión de usuarios"
   echo "2) Gestión de discos"
   echo "3) Gestión de procesos"
   echo "4) Gestión de memoria"
   echo "5) Gestión de texto"
   echo "6) Salir"
   echo
   echo "------------------------------------"
   echo

   read opcion

   case $opcion in
      1) . gestionUsuarios.sh;;
      2) . gestionDiscos.sh;;
      3) . gestionProcesos.sh;;
      4) . gestionMemoria.sh;;
      5) . gestionTexto.sh;;
      6) break;; 
      *) echo "Esta opción no existe vuelva a intentarlo.";;
   esac;done
